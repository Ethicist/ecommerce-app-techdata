# eCommerce app
![This cat has wry smile but he looks very cool](/wrycat.png "Wry Cat")
## Общая информация
- [Идеи реализуемые в приложении](ideas.md)
- [Документация по дизайну, ТЗ](techdoc.md)

## Дизайн
- Макет: [Figma](https://www.figma.com/file/3YMsBd5bKkfFGqlVi7au2G/eCommerce-app)
- [TODO по дизайну](todo.design.md)

## Ресурсы
- _UI:_ [_Ionic Framework_](https://ionic.io/)
- _SDK:_ [_Ionic React_](https://ionicframework.com/docs/react)
- _Prototyping:_ [_Figma_](https://www.figma.com/)
- _Versioning, Git, CI/CO:_ [_GitLab_](https://gitlab.com/)
- _Deployment:_ [_Microsoft AppCenter_](https://appcenter.ms/)
- _iOS testing:_ [_Apple TestFlight_](https://developer.apple.com/testflight/)
- _Android testing:_ [_Microsoft AppCenter_](https://appcenter.ms/)
- _CDN:_ [_Netlify_](https://www.netlify.com/)
- _MIT License description_: [_http://licenseit.ru/wiki/index.php/MIT_License_](http://licenseit.ru/wiki/index.php/MIT_License)
